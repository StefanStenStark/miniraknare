import java.util.List;
import java.util.Scanner;

public class Main {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.println("Booting up Calculator 3000 ...");
        System.out.println("Finished booting up Calculator 3000");

        selectInput();

        boolean inuse = true;

        while(inuse){
            int choice = scanner.nextInt();
            switch (choice){
                case 1:
                    handleDivision();
                case 2:
                    handleDivision();
                case 3:
                    handleDivision();
                case 4:
                    handleDivision();
                case 5:
                    handleDivision();
                case 6:
                    inuse = false;
                    System.out.println("You've selected to quit the calculator. Goodbye!");
                    break;
            }
        }

    }

    public static void selectInput() {

        System.out.println("Type the corresponding number to select your desired operation: ");
        System.out.println("1. Addition");
        System.out.println("2. Subtraction");
        System.out.println("3. Multiplication");
        System.out.println("4. Division");
        System.out.println("5. Square root");
        System.out.println("6. Exit program");
        System.out.println("Your choice: ");


    }

    public static void handleAddition(){

    }

    public static void handleDivision(){
        System.out.println("You've opted for division");
        System.out.println("Enter your numerator: ");
        int numerator = scanner.nextInt();
        System.out.println("Enter your denominator: ");
        int denominator = scanner.nextInt();
        double quotient = Operations.divide(numerator, denominator);
        System.out.println("The result of " + numerator + " divided by " + denominator + " is " + quotient);
    }

}


